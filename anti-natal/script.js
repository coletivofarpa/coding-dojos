var pattern = ['ArrowUp', 'ArrowUp', 'ArrowDown', 'ArrowDown', 'ArrowLeft', 'ArrowRight', 'ArrowLeft', 'ArrowRight', 'b', 'a'];
var current = 0;

var keyHandler = function (event) {

	// If the key isn't in the pattern, or isn't the current key in the pattern, reset
	if (pattern.indexOf(event.key) < 0 || event.key !== pattern[current]) {
		current = 0;
		return;
	}

	// Update how much of the pattern is complete
	current++;

	// If complete, alert and reset
	if (pattern.length === current) {
		current = 0;
		fireClaus()
	}

};

// Listen for keydown events
document.addEventListener('keydown', keyHandler, false);

const audio = new Audio('audio-noel.mp3');
const audioNoel = document.getElementById("audio-noel");

// Faz a magia do fogo acontecer
function fireClaus(){
  document.getElementById("ctn-fire").style.display = "block"  
  audioNoel.play();
  const myTimeout = setTimeout(ocultaNoel, 10000);
  myTimeout()
}

function ocultaNoel() {
  document.getElementById("ctn-fire").style.display = "none";
  audioNoel.stop();
};