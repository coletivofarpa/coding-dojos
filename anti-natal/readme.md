# Coding Dojo - Anti Natal
Para finalizar e celebrar o fim desse ano doido, vamos nos reunir para criar um software web (HTML + CSS + JS) onde simulamos a queima do bom velhinho, simbolo do capitalismo: O Papai Noel

## Proposta criar um "easter eggs" 
De forma colaborativa vamos criar um easter eggs tendo a seguintes premissas

- 1 Usando JS vamos capturar as teclas do teclado do usuario para disparar o evento do easter eggs.
- 2 Ao ser disparado o evento vai aparecer uma fogueira e um papai noel queimando
- 3 Sera disparado uma musica no formato .mp3
- 4 Vamos usar Código Konami, podemos aproveitar algum script JS livre

## Conhece o o que é "easter eggs"

A tradução literal do inglês para o idioma português seria "ovo de páscoa", mas, no mundo virtual "easter eggs" são brincadeiras escondidas em sistemas, ativadas por códigos secretos. Já foi muito utilizado em jogos de vídeo-game, softwares e, claro, invadiu a internet.

Código Konami (também conhecido como Konami Code) é um cheat que pode ser usado em vários videogames da Konami, normalmente habilitando algum tipo de opção secreta.

O código foi utilizado pela primeira vez em 1986 no jogo Gradius para o Nintendo Entertainment System. Durante o jogo, ou nas telas de apresentação, o jogador pressiona a seguinte seqüência no controle:

( ↑ ↑ ↓ ↓ ← → ← → BA ENTER ) e surpresas aparecem.

Então bora lá!